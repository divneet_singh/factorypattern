public interface CrudManager {

    public void create();

    public void read();

    public void update();

    public void delete();

}
