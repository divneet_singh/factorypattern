public class CrudMongo implements CrudManager{
    @Override
    public void create() {
        System.out.println("Mongo Create user");
    }

    @Override
    public void read() {
        System.out.println("Mongo Read details");
    }

    @Override
    public void update() {

        System.out.println("Mongo update details");
    }

    @Override
    public void delete() {

        System.out.println("Mongo delete user");

    }
}
