public class CrudFactory {


    public CrudManager DoCrud(String crudType){
//        System.out.println( crudType.equalsIgnoreCase("test"));
        if(crudType.equalsIgnoreCase("jdbc")){
            return new CrudJdbc();
        }
        else if (crudType.equalsIgnoreCase("Mongo")){

            return new CrudMongo();
        }
//        else {
//            System.out.println("invalid input");
//        }
        return null;
    }

}
