public class Main {

    public static void main(String[] args) {
        CrudFactory factory =new CrudFactory();

        String chooseDb = "mongo";
        CrudManager ctx = factory.DoCrud(chooseDb);
        ctx.create();
        ctx.read();
        ctx.update();
        ctx.delete();
//        factory.DoCrud("mongo");
    }
}
